package dawsoncollege.android.constraintlayoutexample

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import dawsoncollege.android.constraintlayoutexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var whiteDrawable: ColorDrawable
    private lateinit var greyDrawable: ColorDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        whiteDrawable = ColorDrawable(ContextCompat.getColor(baseContext, R.color.white))
        greyDrawable = ColorDrawable(ContextCompat.getColor(baseContext, R.color.grey))

        binding.btn1.setOnClickListener(getButtonHandlerForConstraint(binding.firstConstraint))
        binding.btn2.setOnClickListener(getButtonHandlerForConstraint(binding.firstConstraint))
        binding.btn3.setOnClickListener(getButtonHandlerForConstraint(binding.firstConstraint))

        binding.btn4.setOnClickListener(getButtonHandlerForConstraint(binding.secondConstraint))
        binding.btn5.setOnClickListener(getButtonHandlerForConstraint(binding.secondConstraint))

        binding.btn6.setOnClickListener(getButtonHandlerForConstraint(binding.thirdConstraint))
        binding.btn7.setOnClickListener(getButtonHandlerForConstraint(binding.thirdConstraint))

        binding.btn8.setOnClickListener(getButtonHandlerForConstraint(binding.fourthConstraint))
        binding.btn9.setOnClickListener(getButtonHandlerForConstraint(binding.fourthConstraint))
        binding.btn10.setOnClickListener(getButtonHandlerForConstraint(binding.fourthConstraint))
        binding.btn11.setOnClickListener(getButtonHandlerForConstraint(binding.fourthConstraint))
    }

    private fun getButtonHandlerForConstraint(constraintLayout: ConstraintLayout): (view: View) -> Unit {
        return { _: View ->
            val currentDrawable = constraintLayout.background as ColorDrawable

            constraintLayout.background = if (currentDrawable.color == whiteDrawable.color)
                greyDrawable else whiteDrawable
        }
    }
}